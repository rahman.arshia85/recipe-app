from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import RecipeForm

# Create your views here.
from .models import Recipe


def landingPage(request):
    if request.method == 'POST':
        # transfer info from form print to console
        form = RecipeForm(request.POST)

        if form.is_valid():
            obj = Recipe()
            obj.name = form.cleaned_data['name']
            obj.author = form.cleaned_data['author']
            obj.ingredients = form.cleaned_data['ingredients']
            obj.directions = form.cleaned_data['directions']
            obj.save()
            return HttpResponseRedirect('/')

    else:
        form = RecipeForm()
        data = Recipe.objects.all()

    return render(request, 'index.html', {'form': form, 'data': data})
