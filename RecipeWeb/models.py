from django.db import models


# Create your models here.

class Recipe(models.Model):
    name = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    ingredients = models.CharField(max_length=255)
    directions = models.CharField(max_length=255)
