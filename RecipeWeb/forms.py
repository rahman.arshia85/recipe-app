from django import forms


class RecipeForm(forms.Form):
    name = forms.CharField(label='name', max_length=255)
    author = forms.CharField(label='author', max_length=255)
    ingredients = forms.CharField(label='ingredients', max_length=255)
    directions = forms.CharField(label='directions', max_length=255)