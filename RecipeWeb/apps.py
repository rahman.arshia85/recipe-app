from django.apps import AppConfig


class RecipewebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'RecipeWeb'
